package com.paynearby.test.wardrobe;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

public class ViewImage extends AppCompatActivity {

    String image_path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        // Get image path from closet activity
        image_path = getIntent().getStringExtra("image_path");
        // New SubsamplingScaleImageView
        SubsamplingScaleImageView imageView = findViewById(R.id.imageView);
        // Set Image using URI to ImageView
        imageView.setImage(ImageSource.uri(image_path));
        // Set close action to onBackPressed
        findViewById(R.id.close).setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }
}