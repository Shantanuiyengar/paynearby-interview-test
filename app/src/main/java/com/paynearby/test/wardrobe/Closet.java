package com.paynearby.test.wardrobe;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.paynearby.test.wardrobe.database.AppDatabase;
import com.paynearby.test.wardrobe.database.Pants;
import com.paynearby.test.wardrobe.database.Shirts;
import com.paynearby.test.wardrobe.database.WardrobeAccess;

import java.util.ArrayList;
import java.util.List;

public class Closet extends AppCompatActivity {
    AppDatabase database;
    WardrobeAccess wardrobe;
    List<Shirts> shirts = new ArrayList<>();
    List<Pants> pants = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closet);
        // Set Bottom Navigation
        setBottomNav();
        // Get database reference
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "wardrobe").build();
        // Get Data Access Object
        wardrobe = database.wardrobe();
        // Query to the database using DAO
        new DataFromDatabase().execute();
    }

    private void setBottomNav() {
        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        // Setting current Item as Closet
        bottomNav.setSelectedItemId(R.id.closet);
        // On re-selection of the item, do not reload
        bottomNav.setOnNavigationItemReselectedListener(null);
        // set item click for every menu button
        bottomNav.setOnNavigationItemSelectedListener(item -> {
            if(item.getItemId() == R.id.home){
                startActivity(new Intent(Closet.this,MainActivity.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            else if(item.getItemId() == R.id.favourites){
                startActivity(new Intent(Closet.this,Favourites.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            return true;
        });
    }


    @SuppressLint("StaticFieldLeak")
    class DataFromDatabase extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get shirts from database
            shirts = wardrobe.getAllShirts();
            // Get pants from database
            pants = wardrobe.getAllPants();
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            // Initialize the page
            initWardrobe();
        }
    }

    private void initWardrobe() {
        RecyclerView shirts_,pants_;
        shirts_ = findViewById(R.id.shirts);
        pants_ = findViewById(R.id.pants);
        // Set layout of recycler view to Grids of 3 columns
        shirts_.setLayoutManager(new GridLayoutManager(this,3));
        // Setting Adapter
        shirts_.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_closet,parent,false)) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ImageView image = holder.itemView.findViewById(R.id.image);
                Glide.with(image).load(Uri.parse(shirts.get(position).getUri())).into(image);
                image.setOnClickListener(v -> {
                    // Open Image view on click
                    startActivity(new Intent(Closet.this,ViewImage.class).putExtra("image_path",shirts.get(position).getUri()));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                });
            }

            @Override
            public int getItemCount() {
                return shirts.size();
            }
        });
        // Set layout of recycler view to Grids of 3 columns
        pants_.setLayoutManager(new GridLayoutManager(this,3));
        // Setting Adapter
        pants_.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_closet,parent,false)) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ImageView image = holder.itemView.findViewById(R.id.image);
                Glide.with(image).load(Uri.parse(pants.get(position).getUri())).into(image);
                image.setOnClickListener(v -> {
                    // Open Image view on click
                    startActivity(new Intent(Closet.this,ViewImage.class).putExtra("image_path",pants.get(position).getUri()));
                    overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                });
            }

            @Override
            public int getItemCount() {
                return pants.size();
            }
        });
    }
}