package com.paynearby.test.wardrobe.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Shirts {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "uri")
    public String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

}
