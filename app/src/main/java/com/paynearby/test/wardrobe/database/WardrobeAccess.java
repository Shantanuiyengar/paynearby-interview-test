package com.paynearby.test.wardrobe.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface WardrobeAccess {
    @Query("SELECT * FROM shirts")
    List<Shirts> getAllShirts();

    @Query("SELECT * FROM pants")
    List<Pants> getAllPants();

    @Insert
    void insertShirt(Shirts shirt);

    @Insert
    void insertPant(Pants pant);
}
