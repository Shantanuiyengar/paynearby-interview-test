package com.paynearby.test.wardrobe;

import static androidx.core.content.FileProvider.getUriForFile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.paynearby.test.wardrobe.database.AppDatabase;
import com.paynearby.test.wardrobe.database.LikedCombinations;
import com.paynearby.test.wardrobe.database.Pants;
import com.paynearby.test.wardrobe.database.Shirts;
import com.paynearby.test.wardrobe.database.WardrobeAccess;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    AppDatabase database;
    WardrobeAccess wardrobe;
    List<Shirts> shirts = new ArrayList<>();
    List<Pants> pants = new ArrayList<>();
    Set<String> likedCombinations = new HashSet<>();
    Set<String> randomCombinations = new HashSet<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Get Database Reference
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "wardrobe").build();
        // Get Data Access Object
        wardrobe = database.wardrobe();
        // Get Liked Combinations list
        likedCombinations = new LikedCombinations(this).getLikedCombinations();
        // If permissions granted, move to INIT else Ask for Permissions
        if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            init();
        else
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE},27);
        // Set Bottom Navigation
        setBottomNav();
    }

    BottomNavigationView bottomNav;
    private void setBottomNav() {
        bottomNav = findViewById(R.id.bottom_nav);
        // Set Home as selected Item
        bottomNav.setSelectedItemId(R.id.home);
        // Disabled Reselection
        bottomNav.setOnNavigationItemReselectedListener(null);
        // Set On Menu Item Click
        bottomNav.setOnNavigationItemSelectedListener(item -> {
            if(item.getItemId() == R.id.favourites){
                startActivity(new Intent(MainActivity.this,Favourites.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            else if(item.getItemId() == R.id.closet){
                startActivity(new Intent(MainActivity.this,Closet.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            return true;
        });
    }

    boolean isPermissionAskedAgain = false;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 27){
            ArrayList<String> permissions_denied = new ArrayList<>();
            // Search for denied permissions
            for(String permission : permissions){
                if(checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED)
                    permissions_denied.add(permission);
            }
            // If all Permissions Granted, move to init
            if(permissions_denied.isEmpty()){
                init();
            }
            // Else Ask Missing Permissions Again
            else{
                if(!isPermissionAskedAgain){
                    isPermissionAskedAgain = true;
                    ActivityCompat.requestPermissions(this, permissions_denied.toArray(new String[0]),27);
                }
                // If asked twice and still denied, move user to Settings page to manually enable permissions
                else{
                    Toast.makeText(this,"Permissions Denied. Please enable permissions from Settings",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                    finish();
                }
            }
        }
    }

    CardView favorite;
    ImageView favoriteIcon;
    LikedCombinations liked;
    ViewPager2 allShirts, allPants;
    private void init() {
        favorite = findViewById(R.id.favorite);
        favoriteIcon = findViewById(R.id.favorite_icon);
        allPants = findViewById(R.id.allPants);
        allShirts = findViewById(R.id.allShirts);
        liked = new LikedCombinations(this);
        setClicks();
        new DataFromDatabase().execute();
        // Set Call back for the like button on page change of both View Pagers
        ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                String combination_code = allShirts.getCurrentItem()+" - "+allPants.getCurrentItem();
                if(!liked.getLikedCombinations().contains(combination_code)){
                    favoriteIcon.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.ic_unliked));
                    favoriteIcon.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                }
                else{
                    favoriteIcon.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.ic_liked));
                    favoriteIcon.setBackgroundColor(Color.parseColor("#50FF0000"));
                }
            }
        };
        allPants.registerOnPageChangeCallback(callback);
        allShirts.registerOnPageChangeCallback(callback);
    }

    private void setClicks() {
        // Favourite Button On Click
        favorite.setOnClickListener(v -> {
            String combination_code = allShirts.getCurrentItem()+" - "+allPants.getCurrentItem();
            if(liked.getLikedCombinations().contains(combination_code)){
                favoriteIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_unliked));
                favoriteIcon.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                liked.removeLikedCombination(combination_code);
            }
            else{
                favoriteIcon.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_liked));
                favoriteIcon.setBackgroundColor(Color.parseColor("#50FF0000"));
                liked.addLikedCombination(combination_code);
            }
        });
    }

    String last_random_code;
    private void initWardrobe() {
        // Change UI of shirt section depending on shirt list size
        if(!shirts.isEmpty()){
            findViewById(R.id.choose_shirt_).setVisibility(View.GONE);
            findViewById(R.id.choose_shirt__).setVisibility(View.GONE);
        }
        // Change UI of pant section depending on shirt list size
        if(!pants.isEmpty()){
            findViewById(R.id.choose_pant_).setVisibility(View.GONE);
            findViewById(R.id.choose_pant__).setVisibility(View.GONE);
        }
        // Shirt Add On Click
        findViewById(R.id.add_shirt).setOnClickListener(v -> addPantShirt(1));
        // Pant Add On Click
        findViewById(R.id.add_pant).setOnClickListener(v -> addPantShirt(0));
        // If either are empty, cannot randomize, cannot favourite and thus disabled the bottom nav too.
        if(shirts.isEmpty() || pants.isEmpty()){
            findViewById(R.id.random).setVisibility(View.GONE);
            findViewById(R.id.favorite).setVisibility(View.GONE);
            findViewById(R.id.bottom_nav).setVisibility(View.GONE);
        }
        else{
            findViewById(R.id.random).setVisibility(View.VISIBLE);
            findViewById(R.id.favorite).setVisibility(View.VISIBLE);
            findViewById(R.id.bottom_nav).setVisibility(View.VISIBLE);
        }
        // If Shirts and Pants are more than one, Enable Shuffle
        if(pants.size()>1 || shirts.size()>1)
            findViewById(R.id.random).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.random).setVisibility(View.GONE);
        // Set Shirts Adapter
        allShirts.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image,parent,false)){};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ImageView image = (ImageView) holder.itemView;
                Glide.with(image).load(Uri.parse(shirts.get(position).getUri())).into(image);
            }

            @Override
            public int getItemCount() {
                return shirts.size();
            }
        });
        // Set Pants Adapter
        allPants.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image,parent,false)){};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ImageView image = (ImageView) holder.itemView;
                Glide.with(image).load(Uri.parse(pants.get(position).getUri())).into(image);
            }

            @Override
            public int getItemCount() {
                return pants.size();
            }
        });
        // Set Shuffle logic
        findViewById(R.id.random).setOnClickListener(v -> {
            int shirt;
            int pant;
            // Get Random Shirt
            shirt = new Random().nextInt(shirts.size());
            // Get Random Pant
            pant = new Random().nextInt(pants.size());
            // Create combination and check for combination shown previously and also is not the last combination shown after list clear
            String combination_code = shirt+" - "+pant;
            if(!randomCombinations.contains(combination_code) && !combination_code.equals(last_random_code)){
                allShirts.setCurrentItem(shirt);
                allPants.setCurrentItem(pant);
                last_random_code = combination_code;
                randomCombinations.add(combination_code);
            }
            else{
                int total_combinations = shirts.size() * pants.size();
                // If all combinations are shown, clear list
                if(total_combinations == randomCombinations.size())
                    randomCombinations.clear();
                // Randomize again and ignore same combination as the existing last combination
                findViewById(R.id.random).performClick();
            }
        });
    }

    // Create Dialog for picking from Camera or Gallery
    private void addPantShirt(int pant0_shirt1) {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add an Image");
        builder.setCancelable(false);
        String[] options = {"Camera", "Gallery", "Cancel"};
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    startCamera(pant0_shirt1);
                    break;
                case 1:
                    startGallery(pant0_shirt1);
                    break;
                case 2:
                    dialog.dismiss();
                    break;
            }
            dialog.dismiss();
        });
        builder.create().show();
    }

    // Pick from gallery
    private void startGallery(int pant0_shirt1) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        int request_code = 2710;
        if(pant0_shirt1 == 1)
            request_code = 2711;
        startActivityForResult(intent, request_code);
    }

    String fileName;
    // Pick from Camera
    private void startCamera(int pant0_shirt1){
        if(pant0_shirt1 == 0)
            fileName = "img_"+System.currentTimeMillis() + "_pant.jpg";
        else
            fileName = "img_"+System.currentTimeMillis() + "_shirt.jpg";
        int request_code = 2700;
        if(pant0_shirt1 == 1)
            request_code = 2701;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(fileName));
        try {
            startActivityForResult(takePictureIntent, request_code); } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "Camera Intent Failed..", Toast.LENGTH_SHORT).show();
        }
    }

    // Create new image for camera to store stream in
    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(),"images");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(this, getPackageName() + ".provider", image);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == 2700){
                // Camera Pant
                Uri sourceUri = getCacheImagePath(fileName);
                addPant(sourceUri);
            }
            else if(requestCode == 2701){
                // Camera Shirt
                Uri sourceUri = getCacheImagePath(fileName);
                addShirt(sourceUri);
            }
            else if(requestCode == 2710){
                // Gallery Pant
                Uri sourceUri = data.getData();
                // Create a copy so even if original image is deleted, you can still access it.
                copyToLocal(sourceUri,0);
            }
            else if(requestCode == 2711){
                // Gallery Shirt
                Uri sourceUri = data.getData();
                // Create a copy so even if original image is deleted, you can still access it.
                copyToLocal(sourceUri,1);
            }
        }
    }

    private void copyToLocal(Uri sourceUri, int pant0_shirt1) {
        Glide.with(this).load(sourceUri).into(new CustomTarget<Drawable>() {
            @Override public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                try{
                    File path = new File(getExternalCacheDir(),"images");
                    if (!path.exists()) path.mkdirs();
                    File image = new File(path, "img_"+System.currentTimeMillis() + ".jpg");
                    FileOutputStream stream = new FileOutputStream(image);
                    Bitmap bitmap = ((BitmapDrawable)resource).getBitmap();
                    // Creating copy with 80% quality to save memory but keep image optimized
                    bitmap.compress(Bitmap.CompressFormat.JPEG,80,stream);
                    stream.flush();
                    stream.close();
                    if(pant0_shirt1 == 0)
                        addPant(Uri.fromFile(image));
                    else
                        addShirt(Uri.fromFile(image));
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            @Override public void onLoadCleared(@Nullable Drawable placeholder) {}
        });
    }

    // Add newly added Pant to the list and Database
    @SuppressLint("NotifyDataSetChanged")
    private void addPant(Uri sourceUri) {
        Pants pant =  new Pants();
        pant.setUri(sourceUri.toString());
        new AddShirtPant(null,pant,false).execute();
        pants.add(pant);
        allPants.getAdapter().notifyDataSetChanged();
    }

    // Add newly added Shirt to the list and Database
    @SuppressLint("NotifyDataSetChanged")
    private void addShirt(Uri sourceUri) {
        Shirts shirt =  new Shirts();
        shirt.setUri(sourceUri.toString());
        new AddShirtPant(shirt,null,true).execute();
        shirts.add(shirt);
        allShirts.getAdapter().notifyDataSetChanged();
    }

    // Add Pant or Shirt to the database using Async task so it doesnt stop the main thread
    @SuppressLint("StaticFieldLeak")
    class AddShirtPant extends AsyncTask<Void,Void,Void>{
        Shirts shirt;
        Pants pant;
        boolean isShirt;
        AddShirtPant(Shirts shirt, Pants pant, boolean isShirt){
            this.shirt = shirt;
            this.pant = pant;
            this.isShirt = isShirt;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            if(isShirt)
                wardrobe.insertShirt(shirt);
            else
                wardrobe.insertPant(pant);
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            if(shirts.isEmpty() || pants.isEmpty()){
                findViewById(R.id.random).setVisibility(View.GONE);
                findViewById(R.id.favorite).setVisibility(View.GONE);
                findViewById(R.id.bottom_nav).setVisibility(View.GONE);
            }
            else{
                findViewById(R.id.random).setVisibility(View.VISIBLE);
                findViewById(R.id.favorite).setVisibility(View.VISIBLE);
                findViewById(R.id.bottom_nav).setVisibility(View.VISIBLE);
            }
            if(pants.size()>1 || shirts.size()>1)
                findViewById(R.id.random).setVisibility(View.VISIBLE);
            else
                findViewById(R.id.random).setVisibility(View.GONE);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DataFromDatabase extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get all Shirts
            shirts = wardrobe.getAllShirts();
            // Get all Pants
            pants = wardrobe.getAllPants();
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            // Init UI
            initWardrobe();
        }
    }
}