package com.paynearby.test.wardrobe;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.paynearby.test.wardrobe.database.AppDatabase;
import com.paynearby.test.wardrobe.database.LikedCombinations;
import com.paynearby.test.wardrobe.database.Pants;
import com.paynearby.test.wardrobe.database.Shirts;
import com.paynearby.test.wardrobe.database.WardrobeAccess;

import java.util.ArrayList;
import java.util.List;

public class Favourites extends AppCompatActivity {
    AppDatabase database;
    WardrobeAccess wardrobe;
    List<Shirts> shirts = new ArrayList<>();
    List<Pants> pants = new ArrayList<>();
    List<LikedCombinations.combinationNumbers> likedCombinations = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        setBottomNav();
        // Get Database Reference
        database = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "wardrobe").build();
        // Get Data Access Object
        wardrobe = database.wardrobe();
        // Get Set from SharedPreferences
        likedCombinations = new LikedCombinations(this).getCombinations();
        // Get data from database
        new DataFromDatabase().execute();
        // Load GIF on no favourites
        if(likedCombinations.isEmpty())
            Glide.with(this).load("https://i.imgur.com/2KrbA5r.gif").into((ImageView) findViewById(R.id.loading));
        else
            findViewById(R.id.loading).setVisibility(View.GONE);
    }

    private void setBottomNav() {
        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        // Set current item to favourites
        bottomNav.setSelectedItemId(R.id.favourites);
        bottomNav.setOnNavigationItemReselectedListener(null);
        bottomNav.setOnNavigationItemSelectedListener(item -> {
            if(item.getItemId() == R.id.home){
                startActivity(new Intent(Favourites.this,MainActivity.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            else if(item.getItemId() == R.id.closet){
                startActivity(new Intent(Favourites.this,Closet.class));
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                finish();
            }
            return true;
        });
    }

    @SuppressLint("StaticFieldLeak")
    class DataFromDatabase extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get all shirts
            shirts = wardrobe.getAllShirts();
            // Get all pants
            pants = wardrobe.getAllPants();
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            // Initialize UI
            initWardrobe();
        }
    }

    private void initWardrobe() {
        ViewPager2 pager = findViewById(R.id.favourites);
        // Set list of all favourite items
        pager.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_favs,parent,false)) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ImageView shirt,pant;
                shirt = holder.itemView.findViewById(R.id.shirt);
                pant = holder.itemView.findViewById(R.id.pant);
                Glide.with(shirt).load(Uri.parse(shirts.get(likedCombinations.get(position).getShirt()).getUri())).into(shirt);
                Glide.with(pant).load(Uri.parse(pants.get(likedCombinations.get(position).getPant()).getUri())).into(pant);
            }

            @Override
            public int getItemCount() {
                return likedCombinations.size();
            }
        });
    }
}