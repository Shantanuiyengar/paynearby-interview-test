package com.paynearby.test.wardrobe.database;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LikedCombinations {
    Context context;
    // As database was not mentioned what, I have used Shared Preferences here to store likedCombinations as Set<String>
    public LikedCombinations(Context context){
        this.context = context;
        // Get saved liked combinations
        likedCombinations = context.getSharedPreferences("PayNearbyIn.test.270997",0).getStringSet("likedCombinations",new HashSet<>());
    }

    Set<String> likedCombinations;
    public void addLikedCombination(String code){
        // Add to set
        likedCombinations.add(code);
        // Apply to Shared Preferences
        context.getSharedPreferences("PayNearbyIn.test.270997",0).edit().putStringSet("likedCombinations",likedCombinations).apply();
    }

    public void removeLikedCombination(String code){
        // Remove from set
        likedCombinations.remove(code);
        // Apply to Shared Preferences
        context.getSharedPreferences("PayNearbyIn.test.270997",0).edit().putStringSet("likedCombinations",likedCombinations).apply();
    }

    public Set<String> getLikedCombinations() {
        return likedCombinations;
    }
    List<combinationNumbers> combinations = new ArrayList<>();
    public List<combinationNumbers> getCombinations() {
        // Create a set of shirt and pants combination from code saved.
        for(String set : likedCombinations){
            String[] possibilities = set.split(" - ");
            int shirt = Integer.parseInt(possibilities[0].trim());
            int pant = Integer.parseInt(possibilities[1].trim());
            combinations.add(new combinationNumbers(shirt,pant));
        }
        return combinations;
    }

    public static class combinationNumbers{
        int shirt;
        int pant;

        public combinationNumbers(int shirt, int pant) {
            this.shirt = shirt;
            this.pant = pant;
        }

        public int getShirt() {
            return shirt;
        }

        public int getPant() {
            return pant;
        }
    }
}
